1. Home screen
- Title text
- Fingerprint icon
- Message text to check device's fingeprint availability
- Login button to show fingerprint dialog

![home screen](https://images4.imagebam.com/33/17/cf/ME4KFZO_o.jpg)

2. Fingerprint dialog
- Title text
- Description text
- Fingerprint graphic object

![home screen](https://images4.imagebam.com/2e/0b/13/ME4KFZP_o.jpg)


3. When success (correct fingerprint based on device's enrolled fingerprints)

![home screen](https://images4.imagebam.com/fd/4a/fd/ME4KFZQ_o.jpg)

4. When failed (incorrect fingerprint)

![home screen](https://images4.imagebam.com/03/0a/1b/ME4KFZR_o.jpg)
